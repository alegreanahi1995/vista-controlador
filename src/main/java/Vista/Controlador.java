package Vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.JFileChooser;

import org.apache.http.client.ClientProtocolException;

import modelo.Clima;
import modelo.Fecha;
import modelo.Lugar;
import modelo.Meteorologo;
import modelo.ServicioClima;

import java.nio.file.Files;
public class Controlador implements ActionListener {

private Meteorologo meteorologia;
private View vista;

	public Controlador(Meteorologo model, View vista) {
		this.meteorologia = model;
		this.vista = vista;
		
		this.vista.getBtnBuscar().addActionListener(a->devolverTiempo(a));

	}

	
	public void devolverTiempo(ActionEvent arg0) {
		
		Calendar c	=Calendar.getInstance();
			
		Fecha calendario=new Fecha(c.get(Calendar.DAY_OF_MONTH),c.get(Calendar.MONTH),c.get(Calendar.YEAR));
			Clima t;
			try {
				t = meteorologia.getTiempo(new Lugar(vista.getTxtPais().getText(),vista.getTxtProvincia().getText(),vista.getTxtLocalidad().getText()),calendario );
				String res="Condici�n:"+t.getCondiciones()+ System.lineSeparator()+" T.m�xima:"+t.getMaxTemperatura()+"\n Temperatura:"+t.getTemperatura();
				vista.getTxtResultado().setText(res);
				
			} catch (URISyntaxException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			//model.update(command);
			//view.update(model.getValue());
		}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	
}
