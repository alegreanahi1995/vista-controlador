package Vista;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import javax.swing.*;


/**
 * This is the view part of my MVC implementation of a calculator.
 * It creates the panels and the components of the window.
 * The current value is displayed in a JLabel.
 * 
 * @author Tom Bylander
 */
public class View  {	
	private JLabel lblresultado;
	private JTextField textfield3;
	private JTextArea textfield4;
	private JFrame frame;
    private JPanel panel;
	private TextArea visor;
	public JTextField getTxtPais() {
		return textfield1;
	}

	public JLabel getlblResultado() {
		return lblresultado;
	}




	public void setTxtPais(JTextField textfield1) {
		this.textfield1 = textfield1;
	}





	public JTextField getTxtProvincia() {
		return textfield2;
	}




	

	public void setTxtProvincia(JTextField textfield2) {
		this.textfield2 = textfield2;
	}

	
	
	public JTextField getTxtLocalidad() {
		return textfield3;
	}





	public void setTxtLocalidad(JTextField textfield3) {
		this.textfield3 = textfield3;
	}

	
	
	
	public JTextArea getTxtResultado() {
		return textfield4;
	}





	public void setTxtResultado(JTextField textfield3) {
		this.textfield4 = textfield4;
	}
	private JPanel buttonsPanel;
		    private JTextField textfield1,textfield2;
		    private JButton boton1,boton2,boton3;
		    public View() {

		    	frame = new JFrame();
				frame.setResizable(false);
				frame.setTitle("");
				frame.setBounds(100, 100, 400, 300);
				frame.setAutoRequestFocus(true);
				frame.setLocationRelativeTo(null);
				frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

				buttonsPanel = new JPanel();
				buttonsPanel.setBounds(0, 0, frame.getWidth(), frame.getHeight() - 100);
				frame.getContentPane().add(buttonsPanel);
				buttonsPanel.setLayout(null);



				JLabel lblpais = new JLabel("Pais:"); 
				lblpais.setBounds(5, 10, 88, 21);
				buttonsPanel.add(lblpais);

		        textfield1=new JTextField();
		        textfield1.setBounds(90,10,131,30);
		        buttonsPanel. add(textfield1);
		        
		        JLabel lblprovincia = new JLabel("Provincia:");
		        lblprovincia.setBounds(5, 60, 88, 21);
		        buttonsPanel.add(lblprovincia);
		        textfield2=new JTextField();
		        textfield2.setBounds(90,60,131,30);
		        buttonsPanel.  add(textfield2);
		        JLabel lbllocalidad = new JLabel("Localidad:");
		        lbllocalidad.setBounds(5, 110, 88, 21);
		        buttonsPanel.add(lbllocalidad);
		        textfield3=new JTextField();
		        textfield3.setBounds(90,110,131,30);
		        buttonsPanel.add(textfield3);
		       
		         lblresultado = new JLabel("Resultado: ");
		         lblresultado.setBounds(240, 5, 120, 21);
		         buttonsPanel.add(lblresultado);
				
				textfield4=new JTextArea();
			        textfield4.setBounds(240, 30, 131, 110);
textfield4.setBackground(Color.lightGray);
				       textfield4.setEditable(false);
			        buttonsPanel.	add(textfield4);
				 boton1=new JButton("Buscar");
			        boton1.setBounds(90, 150, 130, 20);
			        buttonsPanel.  add(boton1);
		        
			    

			     
		    }

		    public JButton getBtnBuscar()
		    {
		    	return this.boton1;
		    }
		    
		    public JButton getBtnIngresarProveedor()
		    {
		    	return this.boton2;
		    }
		    
		    public JButton getBtnEliminarProveedor()
		    {
		    	return this.boton3;
		    }
		    
	public void setVisible(boolean estadovisibilidad)
	{
		frame.setVisible(true); //we make the window visible
	      
	}
	/**
	 * Register the controller as the listener to the menu items
	 * and the buttons.
	 * @param controller The event handler for the calculator
	 */
	
		    public void registerListener(Controlador controller) {
		Component[] components = buttonsPanel.getComponents();
		for (Component component : components) {
			if (component instanceof AbstractButton) {
				AbstractButton button = (AbstractButton) component;
				button.addActionListener(controller);
			}
		}
	}
	
	/**
	 * Display the value in the JLabel of the calculator.
	 * Round off the number of digits if needed.
	 * 
	 * @param value the value to be displayed
	 */
/*	public void update(String value) {
		
	}
	
*/

	
}
