package Vista;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import javax.swing.JFrame;

import modelo.Meteorologo;
import modelo.ServicioClima;

public class Main {

	
public static void main(String[] args) throws IOException, InterruptedException {
	
	    View view = new View();
	    String ruta="C:\\Users\\alegr\\Documents\\Servicios";
	    Meteorologo meteorologia=new Meteorologo(ruta);
		Controlador controller = new Controlador(meteorologia, view);
		
		view.registerListener(controller);
	
        view.setVisible(true);
  
}
}
